;;;; papiercomputer.lisp

(cl:defpackage #:papiercomputer
  (:use #:clim #:clim-lisp)
  (:nicknames #:pc)
  (:export #:run))

(cl:in-package #:papiercomputer)

(defclass graphical-view (view)
  ())

(defparameter *graphical-view* (make-instance 'graphical-view))

(defun make-button (label)
  (make-pane 'push-button
             :label label
             :width 50
             :height 20))

(defclass register ()
  ((%number
    :initarg :number
    :initform (error "Ihr Register sollte eine Nummerierung besitzen.")
    :type (integer 1))
   (%state
    :initform 0)))

(defun make-register (number)
  (check-type number (integer 1))
  (make-instance 'register :number number)) 

#+(or) (defun display-register )

(defmacro make-registers (&optional (limit 8))
  (check-type limit (integer 1 8))
  `(tabling (:grid t)
     ,@(loop for i from 1 to limit
             collect `(make-register ,i))))

(define-application-frame papiercomputer ()
  ((%program-state
    :initform (make-array '(99 2) :initial-element nil)))
  (:panes
   (programm :application
             :width :compute
             :height :compute
             :scroll-bars :vertical)
   (einzelschritt (make-button "Einzelschritt"))
   (start (make-button "Start"))
   (ruecksetzen (make-button "Rücksetzen"))
   (register-neu (make-button "Register neu"))
   (programm-neu (make-button "Programm neu"))
   (zeile-einfuegen (make-button "Zeile einfügen"))
   (rechner :application
            :label "Rechner"
            :scroll-bars nil)
   (timer :application
          :label "Timer"
          :scroll-bars nil)
   (darstellung :application
                :label "Darstellung"
                :scroll-bars nil)
   (datenregister :application
                  #+(or) :display-function #+(or) #'display-registers
                  #+(or) :default-view #+(or) *graphical-view*
                  :scroll-bars nil)
   (pointer-documentation :pointer-documentation))
  (:layouts
   (default
    (horizontally ()
      (labelling (:label "Programm")
        (horizontally (:width 550)
          programm
          (vertically (:height 500)
            einzelschritt
            start
            ruecksetzen
            register-neu
            programm-neu
            zeile-einfuegen
            rechner
            timer
            darstellung)))
      (labelling (:label "Datenregister")
        datenregister
        #+(or) (make-registers))))))

(defun run ()
  (run-frame-top-level (make-application-frame 'papiercomputer)))
